<?php
/*
Template Name: Full Width
*/
?>
<?php get_header(); ?>
<?php global $is_home; ?>
		
<?php if ( get_option( 'uniq_breadcrumbs' )) {  ?>
	<div class="breadcrumb">
       <?php yoast_breadcrumb('','');  ?>
	</div>
<?php } ?>	

<div id="fullwidth">
	<h4><span><?php the_title(); ?></span></h4>

 		<?php if(have_posts()) : ?>
			<?php while(have_posts()) : the_post() ?>
            		<?php $pagedesc = get_post_meta($post->ID, 'pagedesc', $single = true); ?>

                    <div id="post-<?php the_ID(); ?>" >
                        <div class="entry"> 
                            <?php the_content(); ?>
                        </div>
                    </div><!--/post-->
                
            <?php endwhile; else : ?>
        
                    <div class="posts">
                        <div class="entry-head"><h2><?php echo get_option('uniq_404error_name'); ?></h2></div>
                        <div class="entry-content"><p><?php echo get_option('uniq_404solution_name'); ?></p></div>
                    </div>
        
        <?php endif; ?>
</div>
<?php get_footer(); ?>
