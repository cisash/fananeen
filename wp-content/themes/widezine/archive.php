<?php get_header(); ?>

<?php if ( get_option( 'uniq_breadcrumbs' )) {  ?>
   <div class="breadcrumb"><?php yoast_breadcrumb('','');  ?></div>
<?php } ?>
    
<div id="the_body">
    <?php if (is_category()) { ?>
    <h4 > <span><?php echo get_option('uniq_browsing_category'); ?> <?php echo single_cat_title(); ?></span> </h4>
    <?php } elseif (is_day()) { ?>
    <h4><span><?php echo get_option('uniq_browsing_day'); ?> <?php the_time('F jS, Y'); ?></span> </h4>
    <?php } elseif (is_month()) { ?>
    <h4><span><?php echo get_option('uniq_browsing_month'); ?>
      <?php the_time('F, Y'); ?></span>
    </h4>
    <?php } elseif (is_year()) { ?>
    <h4><span><?php echo get_option('uniq_browsing_year'); ?>
      <?php the_time('Y'); ?></span>
    </h4>
    <?php } elseif (is_author()) { ?>
    <h4> <span><?php echo get_option('uniq_browsing_author'); ?> <?php echo $curauth->nickname; ?> </span> </h4>
    <?php } elseif (is_tag()) { ?>
    <h4> <span><?php echo get_option('uniq_browsing_tag'); ?> <?php echo single_tag_title('', true); ?>  </span></h4>
    <?php } ?>

    <?php if(have_posts()) : $scounter=0; ?>
      <?php while(have_posts()) : the_post() ?>
	  <?php $scounter++; ?>
      <?php $post_images = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large'); ?>
      
		<div id="post-<?php the_ID(); ?>" class="posts_small <?php if ($scounter==1) echo 'first_small'; ?>">
			<?php if ($post_images[0]) { ?>
				<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>" class="preloader"><img src="<?php echo bloginfo('template_url'); ?>/thumb.php?src=<?php echo $post_images[0];?>&amp;w=200&amp;h=200&amp;zc=1&amp;q=80<?php echo $thumb_url;?>" alt="<?php the_title(); ?>"  class="mag fade_hover" /></a>
			<?php } ?>
									
			<div class="post_content">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<p class="meta">By <?php the_author_posts_link(); ?> | <?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?> | <a href="<?php the_permalink(); ?>#commentarea"><?php comments_number('No comments', 'One comment', '% comments'); ?></a></p>
				<p><?php echo bm_better_excerpt(280, '...'); ?></p>
				<p class="tags">Posted in: <span class="category"><?php the_category(", "); ?> </span></p>
			</div>

		</div>
		
      <?php endwhile; ?>

		<div class="pagination">
			<?php if (function_exists('wp_pagenavi')) { ?>
				<?php wp_pagenavi(); ?>
			<?php } ?>
		</div>
	<?php endif; ?>
      
</div> <!-- the_body end -->
    
<?php get_sidebar(); ?>
<?php get_footer(); ?>
