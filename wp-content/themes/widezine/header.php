<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>
	<?php if ( is_home() ) { ?><?php bloginfo('name'); ?> - <?php bloginfo('description'); ?><?php } ?>
	<?php if ( is_search() ) { ?>Search Results - <?php bloginfo('name'); ?><?php } ?>
	<?php if ( is_author() ) { ?>Author Archives - <?php bloginfo('name'); ?><?php } ?>
	<?php if ( is_single() ) { ?><?php bloginfo('name'); ?> - <?php wp_title(''); ?><?php } ?>
	<?php if ( is_page() ) { ?><?php bloginfo('name'); ?> - <?php wp_title(''); ?><?php } ?>
	<?php if ( is_category() ) { ?><?php single_cat_title(); ?> - <?php bloginfo('name'); ?><?php } ?>
	<?php if ( is_month() ) { ?><?php the_time('F'); ?> - <?php bloginfo('name'); ?><?php } ?>
	<?php if (function_exists('is_tag')) { if ( is_tag() ) { ?><?php bloginfo('name'); ?> - Tag Archive - <?php single_tag_title("", true); } } ?>
</title>

<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<?php if (is_home()) {?>
<?php if ( get_option('uniq_meta_description') <> "" ) { ?>
<meta name="description" content="<?php echo stripslashes(get_option('uniq_meta_description')); ?>" />
<?php } ?>
<?php if ( get_option('uniq_meta_keywords') <> "" ) { ?>
<meta name="keywords" content="<?php echo stripslashes(get_option('uniq_meta_keywords')); ?>" />
<?php } ?>
<?php if ( get_option('uniq_meta_author') <> "" ) { ?>
<meta name="author" content="<?php echo stripslashes(get_option('uniq_meta_author')); ?>" />
<?php } ?>
<?php } ?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" />
<?php if ( get_option('uniq_favicon') <> "" ) { ?>
<link rel="shortcut icon" type="image/png" href="<?php echo get_option('uniq_favicon'); ?>" />
<?php } ?>
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php if ( get_option('uniq_feedburner_url') <> "" ) { echo get_option('uniq_feedburner_url'); } else { echo get_bloginfo_rss('rss2_url'); } ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php if ( get_option('uniq_scripts_header') <> "" ) { echo stripslashes(get_option('uniq_scripts_header')); } ?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/lib/js/prettyPhoto/css/prettyPhoto.css" />
<link href="<?php bloginfo('template_directory'); ?>/lib/css/slimbox.css" rel="stylesheet" type="text/css" />


<?php if ( get_option('uniq_skin') == "Dark" ) { ?>
<link href="<?php bloginfo('template_directory'); ?>/lib/css/style_dark.css" rel="stylesheet" type="text/css" />
<?php } ?>

<!--[if IE 7]>
	<script type='text/javascript' src='<?php bloginfo('template_directory'); ?>/lib/js/excanvas.js'></script>
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/lib/css/IE_fix.css" type="text/css" media="screen" />
<![endif]--> 


<?php if ( get_option('uniq_customcss') ) { ?>
	<link href="<?php bloginfo('template_directory'); ?>/custom.css" rel="stylesheet" type="text/css">
<?php } ?>

<?php if (get_option('uniq_alt_stylesheet') != '') { ?>
	<style type="text/css">
		.req, #fullwidth h4 span, .highlight_light, #the_body h4 span, #sidebar ul.news_list li h3 a:hover, a:hover, h3 a:hover, .category_wise_post ul li a:hover, .related h4 span, #comments h4 span {
			color:#<?php echo get_option('uniq_alt_stylesheet');?>;
		}
	</style>
<?php } else { ?>
	<style type="text/css">
		.req, #fullwidth h4 span, .highlight_light, #the_body h4 span, #sidebar ul.news_list li h3 a:hover, a:hover, h3 a:hover, .category_wise_post ul li a:hover, .related h4 span, #comments h4 span {
			color:#d26630;
		}
	</style>
<?php } ?>

<?php if (get_option('uniq_corners')) { ?>
	<style type="text/css">
	.searchform, #nav, #the_body, .related, #fullwidth, 
	#slider, #sidebar .advt_single img, #sidebar ul.news_list, 
	#sidebar .video, .popular_post ul, #twitter_update_list, 
	#sidebar ul, #sidebar ul.news_list li a img, .posts img,
	.related img, .stylings, img.avatar, 
	#respond input, #respond textarea, #comments, .main_img, img {
		border-radius: 3px;
		-moz-border-radius: 3px;
		-webkit-border-radius: 3px;	
	}
	#topnav ul ul, #topnav ul ul ul, #nav ul ul, #nav ul ul ul {
		-moz-border-radius-bottomleft:3px;
		-moz-border-radius-bottomright:3px;
		-webkit-border-bottom-left-radius:3px;
		-webkit-border-bottom-right-radius:3px;
	}
	</style>
<?php } ?>


<?php global $cufon_disable; if (get_option('uniq_cufon')) { $cufon_disable=true; }  ?>

<?php wp_head(); ?>

<?php if ( get_option('uniq_analytics') <> "" ) { echo stripslashes(get_option('uniq_analytics')); } ?>
</head>

<body <?php body_class(); ?>>
	<div class="main-container">
		<div id="header">

			<div id="logo">
				<?php if ( get_option('uniq_show_blog_title') ) { ?>
                   <div class="blog-title"><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a> 
                   		
                         <p class="blog-description">
                          <?php bloginfo('description'); ?>
                        </p>
                   	
                   </div> 
                <!-- default logo -->
                <?php } else { ?>
					<?php if (get_option('uniq_skin') == "Light") { ?>
						<a href="<?php echo get_option('home'); ?>/">
						<img src="<?php if ( get_option('uniq_logo_url') <> "" ) { echo get_option('uniq_logo_url'); } else { echo get_bloginfo('template_directory').'/assets/logo.png'; } ?>" alt="<?php bloginfo('name'); ?>" class="logo"  /></a>
					<?php } else { ?>
						<a href="<?php echo get_option('home'); ?>/">
						<img src="<?php if ( get_option('uniq_logo_url') <> "" ) { echo get_option('uniq_logo_url'); } else { echo get_bloginfo('template_directory').'/assets/logo_dark.png'; } ?>" alt="<?php bloginfo('name'); ?>" class="logo"  /></a>
					<?php } ?>
				<?php } ?>
            </div>
                

			<div id="topnav">
				<ul class="sf-menu">    
					
						<?php wp_list_pages('title_li=&depth=0&exclude=947,' . get_inc_pages("pag_exclude_") .'&sort_column=menu_order');  ?>
				</ul>
				<?php DISPLAY_ACURAX_ICONS(); ?>
			</div>

		 
		</div> <!-- bottom -->
		<div class="clear"></div>

	<div id="container">