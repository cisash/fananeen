<?php
// Do not change this unless you know what you are doing
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>x
		<p class="nocomments"><?php echo get_option('uniq_password_protected_name'); ?></p>
	<?php
		return;
	}
?>


<div id="comments_wrap">
	<?php if (have_comments()): ?>
		<h4><span><?php comments_number(''.get_option('uniq_comment_responsesa_name').'', ''.get_option('uniq_comment_responsesb_name').'', ''.get_option('uniq_comment_responsesc_name').'' );?></span></h4>

		<ol class="commentlist">
			<?php wp_list_comments('avatar_size=80&callback=custom_comment'); ?>
		</ol>    

		<div class="navigation">
			<div class="alignleft"><?php previous_comments_link() ?></div>
			<div class="alignright"><?php next_comments_link() ?></div>	
		</div>
	
		<?php if ($comments_by_type['pings']): ?>
			<h4 id="pings"><span><?php echo get_option('uniq_comment_trackbacks_name'); ?></span></h4>
			
			<ol class="commentlist">
				<?php wp_list_comments('type=pings'); ?>
			</ol>
			
		<?php endif; ?>

	<?php else : // this is displayed if there are no comments so far ?>

		<?php if ('open' == $post->comment_status) : ?>
			<!-- If comments are open, but there are no comments. -->

		<?php else : // comments are closed ?>
		 
			<!-- If comments are closed. -->
			<p class="nocomments"><?php echo get_option('uniq_comment_closed_name'); ?></p>

		<?php endif; ?>

	<?php endif; ?>
</div> <!-- end #comments_wrap -->

<?php if ('open' == $post->comment_status): ?>
<div id="respond">
    <h4><span><?php comment_form_title( ''.get_option('uniq_comment_reply_name').'', ''.get_option('uniq_comment_reply_name').' &rarr; %s' ); ?></span></h4>
    <div class="cancel-comment-reply">
		<small><?php cancel_comment_reply_link(); ?></small>
	</div>

    <?php if ( get_option('comment_registration') && !$user_ID ) : ?>
        <p><?php echo get_option('uniq_comment_mustbe_name'); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>"><?php echo get_option('uniq_comment_loggedin_name'); ?></a> <?php echo get_option('uniq_comment_postcomment_name'); ?></p>
    <?php else : ?>
        <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
            <?php if ( $user_ID ) : ?>
                <p><?php echo get_option('uniq_comment_loggedin_name'); ?> &rarr; <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(); ?>" title="Log out of this account"><?php echo get_option('uniq_comment_logout_name'); ?> &raquo;</a></p>
            <?php else : ?>
                <p><input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="1" class="name" />
				<label for="author"><small><?php echo get_option('uniq_comment_name_name'); ?><span class="req"><?php if ($req) _e('*'); ?></span></small></label></span></p>
				
                <p><input type="text" name="email" id="email" value="<?php echo $comment_auth_email; ?>" size="22" tabindex="2" class="email" />
				<label for="email"><small><?php echo get_option('uniq_comment_mail_name'); ?><span class="req"><?php if ($req) _e('*'); ?></span></small></label></span> </p>

				<p class="commpadd"><input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="22" tabindex="3" class="website" />
			    <label for="url"><small><?php echo get_option('uniq_comment_website_name'); ?></small></label> </span></p>
            <?php endif; ?>

			<p><textarea name="comment" id="comment" rows="10" cols="10" tabindex="4"></textarea></p>

			<div class="aleft" ><input name="submit" type="submit" id="submit" tabindex="5" value="<?php echo get_option('uniq_comment_addcomment_name'); ?>" />
				<?php comment_id_fields(); ?>
			</div>
			
		<?php do_action('comment_form', $post->ID); ?>
        </form>

    <?php endif; // If logged in ?>
	
</div> <!-- end #respond -->
<?php endif; // if you delete this the sky will fall on your head ?>
