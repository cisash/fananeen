<?php
/*
Template Name: Sitemap Page
*/
?>
<?php get_header(); ?>
	<?php $is_page = true; ?>

	<?php if ( get_option( 'uniq_breadcrumbs' )) {  ?>
                    <div class="breadcrumb">
                    <?php yoast_breadcrumb('','');  ?>
                    </div>
                <?php } ?>

       		<div id="the_body">			 

                
                  
                 
     	
		<div id="post-<?php the_ID(); ?>" class="post archive-spot">

        <h4><span>Pages</span></h4>
        <ul>
          <?php wp_list_pages('title_li='); ?>
        </ul>

	  <hr />

        <h4><span>Latest Posts</h4>
        <ul>
          <?php $archive_query = new WP_Query('showposts=60');
		                        
								while ($archive_query->have_posts()) : $archive_query->the_post(); ?>
          <li><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
            <?php the_title(); ?>
            </a> <strong>
            <?php comments_number('0', '1', '%'); ?>
            </strong></li>
          <?php endwhile; ?>
        </ul>

		<hr />
		
        <h4><span>Monthly Archives</span></h4>
        <ul>
          <?php wp_get_archives('type=monthly'); ?>
        </ul>

		<hr />

        <h4><span>Categories</span></h4>
        <ul>
          <?php wp_list_categories('title_li=&hierarchical=0&show_count=1') ?>
        </ul>

		<hr />
		
        <h4><span>RSS Feeds</span></h4>
        <ul>
          <li><a href="<?php bloginfo('rdf_url'); ?>" title="RDF/RSS 1.0 feed">RDF / RSS 1.0 feed</a></li>
          <li><a href="<?php bloginfo('rss_url'); ?>" title="RSS 0.92 feed">RSS 0.92 feed</a></li>
          <li><a href="<?php bloginfo('rss2_url'); ?>" title="RSS 2.0 feed">RSS 2.0 feed</a></li>
          <li><a href="<?php bloginfo('atom_url'); ?>" title="Atom feed">Atom feed</a></li>
        </ul>

    </div>
        
	   
      </div> <!-- content  #end -->
        
        
		  <?php get_sidebar(); ?>
	
<?php get_footer(); ?>
