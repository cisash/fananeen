<?php

$themename = "WideZine";
$shortname = "uniq";
$template = get_option('template');
$customcssurl = "".trailingslashit( get_bloginfo('url') )."wp-admin/theme-editor.php?file=/themes/$template/custom.css&theme=$current_theme&dir=style";
$breadcrumbsurl = "".trailingslashit( get_bloginfo('url') )."wp-admin/options-general.php?page=yoast-breadcrumbs.php";
$generaloptionsurl = "".trailingslashit( get_bloginfo('url') )."wp-admin/options-general.php";
$widgetsurl = "".trailingslashit( get_bloginfo('url') )."wp-admin/widgets.php";
$bloghomeurl = "".trailingslashit( get_bloginfo('url') )."";

  $functions_path = TEMPLATEPATH . '/lib/functions/';
  $includes_path = TEMPLATEPATH . '/lib/includes/';
  $javascript_path = TEMPLATEPATH . '/lib/js/';
  $css_path = TEMPLATEPATH . '/lib/css/';

?>
