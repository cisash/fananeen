<?php

// Register widgetized areas
if ( function_exists('register_sidebar') ) {
   register_sidebars(1,array('name' => 'Header AD','before_widget' => '<div class="widget">','after_widget' => '</div>','before_title' => '<h4><span>','after_title' => '</span></h4>'));
	 register_sidebars(1,array('name' => 'Front content','before_widget' => '<div class="widget">','after_widget' => '</div>','before_title' => '<h4><span>','after_title' => '</span></h4>'));
	 register_sidebars(1,array('name' => 'Sidebar','before_widget' => '<div class="widget">','after_widget' => '</div>','before_title' => '<h4><span>','after_title' => '</span></h4>'));
	 register_sidebars(4,array('name' => 'Footer column %d ','before_widget' => '<div class="widget">','after_widget' => '</div>','before_title' => '<h4><span>','after_title' => '</span></h4>'));
	 register_sidebars(1,array('name' => 'About paintings','before_widget' => '<div class="widget">','after_widget' => '</div>','before_title' => '<h4><span>','after_title' => '</span></h4>'));
	 register_sidebars(1,array('name' => 'Search Artists','before_widget' => '<div class="widget">','after_widget' => '</div>','before_title' => '<h4><span>','after_title' => '</span></h4>'));
	 register_sidebars(1,array('name' => 'Artist page','before_widget' => '<div class="widget">','after_widget' => '</div>','before_title' => '<h4><span>','after_title' => '</span></h4>'));
	 register_sidebars(1,array('name' => 'Contact','before_widget' => '<div class="widget">','after_widget' => '</div>','before_title' => '<h4><span>','after_title' => '</span></h4>'));
	 register_sidebars(1,array('name' => 'about footer','before_widget' => '<div class="fb-footer">','after_widget' => '</div>','before_title' => '<h4><span>','after_title' => '</span></h4>'));
}
	
// Check for widgets in widget-ready areas http://wordpress.org/support/topic/190184?replies=7#post-808787
// Thanks to Chaos Kaizer http://blog.kaizeku.com/
function is_sidebar_active( $index = 1){
	$sidebars	= wp_get_sidebars_widgets();
	$key		= (string) 'sidebar-'.$index;
 
	return (isset($sidebars[$key]));
}

// ===============================  Advertise widget  ======================================
class advtwidget extends WP_Widget {
	function advtwidget() {
	//Constructor
		$widget_ops = array('classname' => 'widget Advertise', 'description' => 'Common advertise widget (to be used in any widget slot)' );		
		$this->WP_Widget('advtwidget', 'Uniq &rarr; Advertise', $widget_ops);
	}
	function widget($args, $instance) {
	// prints the widget
		extract($args, EXTR_SKIP);
		$title = empty($instance['title']) ? '&nbsp;' : apply_filters('widget_title', $instance['title']);
		$desc1 = empty($instance['desc1']) ? '&nbsp;' : apply_filters('widget_desc1', $instance['desc1']);
		 ?>						


		 
  		<?php /*?><h4><?php echo $title; ?> </h4><?php */?>
          
        <div class="advt_single">    
        <?php if ( $desc1 <> "" ) { ?>	
         <?php echo $desc1; ?> 
         <?php } ?>
        </div>        
	 
             
	<?php
	}
	function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;		
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['desc1'] = ($new_instance['desc1']);
		return $instance;
	}
	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 't1' => '', 't2' => '', 't3' => '',  'img1' => '', 'desc1' => '' ) );		
		$title = strip_tags($instance['title']);
		$desc1 = ($instance['desc1']);
?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>">Widget Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
     
        <p><label for="<?php echo $this->get_field_id('desc1'); ?>">Advertise code (eg. AdSense, BuySellAds, simple HTML): <textarea class="widefat" rows="6" cols="20" id="<?php echo $this->get_field_id('desc1'); ?>" name="<?php echo $this->get_field_name('desc1'); ?>"><?php echo attribute_escape($desc1); ?></textarea></label></p>
       
<?php
	}}
register_widget('advtwidget');

 
// =============================== Latest Posts Widget (particular category) ======================================

class LatestPosts extends WP_Widget {
	function LatestPosts() {
	//Constructor
		$widget_ops = array('classname' => 'widget latest posts', 'description' => 'Slider listing the latest posts in a particular category. To be used only on FRONT CONTENT widget area only.' );
		$this->WP_Widget('widget_posts1', 'Uniq &rarr; Slider &rarr; Latest Posts', $widget_ops);
	}

	function widget($args, $instance) {
	// prints the widget

		extract($args, EXTR_SKIP);
		echo $before_widget;
		$title = empty($instance['title']) ? '&nbsp;' : apply_filters('widget_title', $instance['title']);
 		$category = empty($instance['category']) ? '&nbsp;' : apply_filters('widget_category', $instance['category']);
		$post_number = empty($instance['post_number']) ? '&nbsp;' : apply_filters('widget_post_number', $instance['post_number']);
		$post_link = empty($instance['post_link']) ? '&nbsp;' : apply_filters('widget_post_link', $instance['post_link']);
		?>
		<h4><span><?php echo $title; ?></span></h4>
		<?php // if ( !empty( $title ) ) { echo $before_title . $title . $after_title; };
		echo '<div id="slider">';
		 ?>
                
                 	 <ul> 
				<?php 
			        global $post;
			        $latest_menus = get_posts('numberposts='.$post_number.'postlink='.$post_link.'&category='.$category.'');
                    foreach($latest_menus as $post) :
                    setup_postdata($post);
			    ?>
                 <?php $post_images = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large'); ?>
           
           			 
          		
   				 
            
             			  <li>
                    
                    
                    <?php if($post_images[0]){ global $thumb_url; ?>
						<a class="preloader" href="<?php the_permalink(); ?>">   <img src="<?php echo bloginfo('template_url'); ?>/thumb.php?src=<?php echo $post_images[0];?>&amp;w=700&amp;h=290&amp;zc=1&amp;q=80<?php echo $thumb_url;?>" alt="" title=""  /> </a>
            <?php }?>   
                      <h2><a class="widget-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                 <p class="date"><?php the_time('F j Y') ?>, at <?php the_time('H:s A') ?>, <a href="<?php the_permalink(); ?>#commentarea"><?php comments_number('No Comments', '1 Comment', '% Comments'); ?> </a>  </p>
					</li>
     
   
<?php endforeach; ?>
                 
    
   

<?php

	    echo '</ul> <div id="slidenav"></div></div>';

		echo $after_widget;
	}

	function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['category'] = strip_tags($new_instance['category']);
		$instance['post_number'] = strip_tags($new_instance['post_number']);
		$instance['post_link'] = strip_tags($new_instance['post_link']);
		return $instance;

	}

	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'category' => '', 'post_number' => '' ) );
		$title = strip_tags($instance['title']);
		$category = strip_tags($instance['category']);
		$post_number = strip_tags($instance['post_number']);
		$post_link = strip_tags($instance['post_link']);

?>
<p>
  <label for="<?php echo $this->get_field_id('title'); ?>">Title:
    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" />
  </label>
</p>
<p>
  <label for="<?php echo $this->get_field_id('category'); ?>">Categories (<code>IDs</code> separated by commas):
  <input class="widefat" id="<?php echo $this->get_field_id('category'); ?>" name="<?php echo $this->get_field_name('category'); ?>" type="text" value="<?php echo attribute_escape($category); ?>" />
  </label>
</p>
<p>
  <label for="<?php echo $this->get_field_id('post_number'); ?>">Number of posts:
  <input class="widefat" id="<?php echo $this->get_field_id('post_number'); ?>" name="<?php echo $this->get_field_name('post_number'); ?>" type="text" value="<?php echo attribute_escape($post_number); ?>" />
  </label>
</p>
<?php
	}

}

register_widget('LatestPosts');



// =============================== Latest news posts Widget (particular category) ======================================

class news2columns extends WP_Widget {
	function news2columns() {
	//Constructor
		$widget_ops = array('classname' => 'widget Latest News', 'description' => 'List of latest posts in a particular category. Widget to be used only on SIDEBAR or FOOTER.' );
		$this->WP_Widget('news2columns', 'Uniq &rarr; Latest News', $widget_ops);
	}

	function widget($args, $instance) {
	// prints the widget

		extract($args, EXTR_SKIP);
		echo $before_widget;
		$title = empty($instance['title']) ? '&nbsp;' : apply_filters('widget_title', $instance['title']);
 		$category = empty($instance['category']) ? '&nbsp;' : apply_filters('widget_category', $instance['category']);
		$post_number = empty($instance['post_number']) ? '&nbsp;' : apply_filters('widget_post_number', $instance['post_number']);
		$post_link = empty($instance['post_link']) ? '&nbsp;' : apply_filters('widget_post_link', $instance['post_link']);

		// if ( !empty( $title ) ) { echo $before_title . $title . $after_title; };
		echo '';
		 ?>
          <h4><span><?php echo $title; ?></span></h4>
          <ul class="news_list">
                
				<?php 
			        global $post; $i=0;
			        $latest_menus = get_posts('numberposts='.$post_number.'postlink='.$post_link.'&category='.$category.'');
                    foreach($latest_menus as $post) :
                    setup_postdata($post);
			    ?>
                 <?php $post_images = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large'); ?>
				
				<?php if ($i ==0) { ?>
					<li class="first">
				<?php } else if ($i==$post_number-1) { ?>
					<li class="last">
				<?php } else { ?>
					<li>
				<?php } $i++;?>
            	<?php if($post_images[0]){ global $thumb_url;?>
                   <a class="widget-title fade_hover preloader" href="<?php the_permalink(); ?>"><img src="<?php echo bloginfo('template_url'); ?>/thumb.php?src=<?php echo $post_images[0];?>&amp;w=60&amp;h=60&amp;zc=1&amp;q=80<?php echo $thumb_url;?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" /> </a>  
            <?php }?>
					<div class="rights">
						<h3> <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3> 
						<p class="meta"><?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?></p>
					</div>
				</li>
    
<?php endforeach; ?>
<?php

	    echo '</ul>';

		echo $after_widget;
	}

	function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['category'] = strip_tags($new_instance['category']);
		$instance['post_number'] = strip_tags($new_instance['post_number']);
		$instance['post_link'] = strip_tags($new_instance['post_link']);
		return $instance;

	}

	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'category' => '', 'post_number' => '' ) );
		$title = strip_tags($instance['title']);
		$category = strip_tags($instance['category']);
		$post_number = strip_tags($instance['post_number']);
		$post_link = strip_tags($instance['post_link']);

?>
<p>
  <label for="<?php echo $this->get_field_id('title'); ?>">Title:
    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" />
  </label>
</p>
<p>
  <label for="<?php echo $this->get_field_id('category'); ?>">Categories (<code>IDs</code> separated by commas):
  <input class="widefat" id="<?php echo $this->get_field_id('category'); ?>" name="<?php echo $this->get_field_name('category'); ?>" type="text" value="<?php echo attribute_escape($category); ?>" />
  </label>
</p>
<p>
  <label for="<?php echo $this->get_field_id('post_number'); ?>">Number of posts:
  <input class="widefat" id="<?php echo $this->get_field_id('post_number'); ?>" name="<?php echo $this->get_field_name('post_number'); ?>" type="text" value="<?php echo attribute_escape($post_number); ?>" />
  </label>
</p>
<?php
	}

}

register_widget('news2columns');




// =============================== category wise news list Widget (particular category) ======================================

class CategoryNews extends WP_Widget {
	function CategoryNews() {
	//Constructor
		$widget_ops = array('classname' => 'widget Category wise News List', 'description' => 'List of latest posts in a particular category. To be used only on FRONT CONTENT area.' );
		$this->WP_Widget('CategoryNews', 'Uniq &rarr; Latest News - Category (Front only)', $widget_ops);
	}

	function widget($args, $instance) {
	// prints the widget

		extract($args, EXTR_SKIP);
		echo $before_widget;
		$title = empty($instance['title']) ? '&nbsp;' : apply_filters('widget_title', $instance['title']);
 		$category = empty($instance['category']) ? '&nbsp;' : apply_filters('widget_category', $instance['category']);
		$post_number = empty($instance['post_number']) ? '&nbsp;' : apply_filters('widget_post_number', $instance['post_number']);
		$post_link = empty($instance['post_link']) ? '&nbsp;' : apply_filters('widget_post_link', $instance['post_link']);

		// if ( !empty( $title ) ) { echo $before_title . $title . $after_title; };
		echo ''; global $globi;
		 ?>
		 
		 <?php if ($globi % 2 == 0) { ?>
		<div class="one_half">
		<?php } else { ?>
		<div class="one_half last">
		<?php } $globi++; ?>
          <h4> <span><?php echo $title; ?></span> </h4>
          
               <div class="category_wise_post">   
                <?php
				 global $post;
				 setup_postdata($post);
				$postcount = 0;
				$page = (get_query_var('paged')) ? get_query_var('paged') : 1;
				query_posts( 'paged=$page&showposts='.$post_number.'&cat='.$category );
				if (have_posts()) { while (have_posts()) { the_post(); 
				if( $postcount == 0 ) { 
				//GETS LATEST POST ?>
     	<?php $post_images = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large'); ?>
 			
			  
            <?php if($post_images[0]){ global $thumb_url; ?>
                   <div class="cate_img"><a class="popular_img preloader" href="<?php the_permalink(); ?>"><img src="<?php echo bloginfo('template_url'); ?>/thumb.php?src=<?php echo $post_images[0];?>&amp;w=345&amp;h=180&amp;zc=1&amp;q=80<?php echo $thumb_url;?>" alt="" title="" class="fade_hover" /></a></div>
                   <h3><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
                     
                     <p class="meta">By <?php the_author_posts_link(); ?> | <?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?> | <a href="<?php the_permalink(); ?>#commentarea"><?php comments_number('No comments', 'One comment', '% comments'); ?></a></p>            
                    <?php the_excerpt(); ?>
            <?php }?>  
            
            
  
  
  			<ul>
  			<?php 
		} elseif( $postcount > 0 && $postcount <= 5 ) { 
		//GETS MORE EXCERUniqS
	?>
	 
    <li> <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a> </li>  
    
	<?php }	$postcount ++;	// close the loop 
		} 
	 } ?>        
    	
        </ul>
    </div>

<?php

	    echo '</div>';

		echo $after_widget;
	}

	function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['category'] = strip_tags($new_instance['category']);
		$instance['post_number'] = strip_tags($new_instance['post_number']);
		$instance['post_link'] = strip_tags($new_instance['post_link']);
		return $instance;

	}

	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'category' => '', 'post_number' => '' ) );
		$title = strip_tags($instance['title']);
		$category = strip_tags($instance['category']);
		$post_number = strip_tags($instance['post_number']);
		$post_link = strip_tags($instance['post_link']);

?>
<p>
  <label for="<?php echo $this->get_field_id('title'); ?>">Title:
    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" />
  </label>
</p>
<p>
  <label for="<?php echo $this->get_field_id('category'); ?>">Categories (<code>IDs</code> separated by commas):
  <input class="widefat" id="<?php echo $this->get_field_id('category'); ?>" name="<?php echo $this->get_field_name('category'); ?>" type="text" value="<?php echo attribute_escape($category); ?>" />
  </label>
</p>
<p>
  <label for="<?php echo $this->get_field_id('post_number'); ?>">Number of posts:
  <input class="widefat" id="<?php echo $this->get_field_id('post_number'); ?>" name="<?php echo $this->get_field_name('post_number'); ?>" type="text" value="<?php echo attribute_escape($post_number); ?>" />
  </label>
</p>
<?php
	}

}

register_widget('CategoryNews');


// =============================== category wise news list Widget (particular category) ======================================

class NoThumbNews extends WP_Widget {
	function NoThumbNews() {
	//Constructor
		$widget_ops = array('classname' => 'widget Category wise News List', 'description' => 'List of latest posts in a particular category. To be used only on FRONT CONTENT area.' );
		$this->WP_Widget('NoThumbNews', 'Uniq &rarr; Latest News - NoThumbs (Front only)', $widget_ops);
	}

	function widget($args, $instance) {
	// prints the widget

		extract($args, EXTR_SKIP);
		echo $before_widget;
		$title = empty($instance['title']) ? '&nbsp;' : apply_filters('widget_title', $instance['title']);
 		$category = empty($instance['category']) ? '&nbsp;' : apply_filters('widget_category', $instance['category']);
		$post_number = empty($instance['post_number']) ? '&nbsp;' : apply_filters('widget_post_number', $instance['post_number']);
		$post_link = empty($instance['post_link']) ? '&nbsp;' : apply_filters('widget_post_link', $instance['post_link']);

		// if ( !empty( $title ) ) { echo $before_title . $title . $after_title; };
		echo ''; global $globi;
		 ?>
		 
		 <?php if ($globi % 2 == 0) { ?>
		<div class="one_half">
		<?php } else { ?>
		<div class="one_half last">
		<?php } $globi++; ?>
          <h4> <span><?php echo $title; ?></span> </h4>
          
               <div class="category_wise_post">   
                <?php
				 global $post;
				 echo "<ul style='border-top:0px;'>";
				 setup_postdata($post);
				$postcount = 0;
				$page = (get_query_var('paged')) ? get_query_var('paged') : 1;
				query_posts( 'paged=$page&showposts='.$post_number.'&cat='.$category );
				if (have_posts()) { while (have_posts()) { the_post(); 
				
				if( $postcount >= 0 && $postcount <= 5 ) { 
		//GETS MORE EXCERUniqS
	?>
	 
    <li> <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a> </li>  
    
	<?php }	$postcount ++;	// close the loop 
		} 
	 } ?>        
    	
        </ul>
    </div>

<?php

	    echo '</div>';

		echo $after_widget;
	}

	function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['category'] = strip_tags($new_instance['category']);
		$instance['post_number'] = strip_tags($new_instance['post_number']);
		$instance['post_link'] = strip_tags($new_instance['post_link']);
		return $instance;

	}

	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'category' => '', 'post_number' => '' ) );
		$title = strip_tags($instance['title']);
		$category = strip_tags($instance['category']);
		$post_number = strip_tags($instance['post_number']);
		$post_link = strip_tags($instance['post_link']);

?>
<p>
  <label for="<?php echo $this->get_field_id('title'); ?>">Title:
    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" />
  </label>
</p>
<p>
  <label for="<?php echo $this->get_field_id('category'); ?>">Categories (<code>IDs</code> separated by commas):
  <input class="widefat" id="<?php echo $this->get_field_id('category'); ?>" name="<?php echo $this->get_field_name('category'); ?>" type="text" value="<?php echo attribute_escape($category); ?>" />
  </label>
</p>
<p>
  <label for="<?php echo $this->get_field_id('post_number'); ?>">Number of posts:
  <input class="widefat" id="<?php echo $this->get_field_id('post_number'); ?>" name="<?php echo $this->get_field_name('post_number'); ?>" type="text" value="<?php echo attribute_escape($post_number); ?>" />
  </label>
</p>
<?php
	}

}

register_widget('NoThumbNews');




// =============================== 1 columns news posts Widget (particular category) ======================================

class news1columns extends WP_Widget {
	function news1columns() {
	//Constructor
		$widget_ops = array('classname' => 'widget Single Columns News', 'description' => 'List of latest posts (as seen on archive pages). Only for FRONT CONTENT area!' );
		$this->WP_Widget('news1columns', 'Uniq &rarr; Latest Posts 1 Column', $widget_ops);
	}

	function widget($args, $instance) {
	// prints the widget

		extract($args, EXTR_SKIP);
		echo $before_widget;
		$title = empty($instance['title']) ? '&nbsp;' : apply_filters('widget_title', $instance['title']);
 		$category = empty($instance['category']) ? '&nbsp;' : apply_filters('widget_category', $instance['category']);
		$post_number = empty($instance['post_number']) ? '&nbsp;' : apply_filters('widget_post_number', $instance['post_number']);
		$post_link = empty($instance['post_link']) ? '&nbsp;' : apply_filters('widget_post_link', $instance['post_link']);

		// if ( !empty( $title ) ) { echo $before_title . $title . $after_title; };
		echo '';
		 ?>
				<?php 
			        global $post;
			        $latest_menus = get_posts('numberposts='.$post_number.'postlink='.$post_link.'&category='.$category.'');

			    ?>

			    <ul class="about-slides">
			    <?php
                    foreach($latest_menus as $post) :
                    setup_postdata($post);
			    ?>
                 <?php $post_images = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large'); ?>
           		<li>
           		<div class="posts_small">
					<?php 
					foreach((get_the_category()) as $category) {
						if ($category->term_id != 8)
    					$category_name = $category->cat_name . ' ';
					}
					
					if ($post_images[0]) { ?>
					<div class="mag preloader">
						<a href="<?php the_permalink() ?>" rel="bookmark" title="Click to view all Painting information"><img src="<?php echo bloginfo('template_url'); ?>/thumb.php?src=<?php echo $post_images[0];?>&amp;w=200&amp;h=200&amp;zc=1&amp;q=80<?php echo $thumb_url;?>" alt="<?php the_title(); ?>"  class="mag fade_hover" />
							<?php /*<div class="artist-name">
							<?php echo $category_name;?>
							</div>*/ ?>
						</a>
					</div>
					<?php } ?>
				</div>
				</li>
				<?php endforeach; ?>
			</ul>
				<ul id="slider0" class="about-slide-item"></ul>
		    <ul id="slider1" class="about-slide-item"></ul>
		    <ul id="slider2" class="about-slide-item"></ul>
		    <ul id="slider3" class="about-slide-item"></ul>
		    <ul id="slider4" class="about-slide-item"></ul>
		    <ul id="slider5" class="about-slide-item"></ul>


		    <script type="text/javascript">
		    jQuery(window).load(function (){
		    	divs = jQuery('.about-slides li');
		    	var count = 0,current=0;
		      (function slide(start){
				    current++;
				    current = current % divs.length;
				    count = ++count % 6;
				    jQuery('#slider'+count).data('current', current).fadeOut('slow', function() {
				      jQuery(this).html(jQuery(divs[jQuery(this).data('current')]).clone()).fadeIn('slow');
				    });

				    if (start < 6) {
				      slide(++start);
				    } else {
				      setTimeout(function() {slide(9);}, 3000);
				    }
				  })(0);
				});
		    </script>





<?php

	    echo '</div>';

		echo $after_widget;
	}

	function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['category'] = strip_tags($new_instance['category']);
		$instance['post_number'] = strip_tags($new_instance['post_number']);
		$instance['post_link'] = strip_tags($new_instance['post_link']);
		return $instance;

	}

	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'category' => '', 'post_number' => '' ) );
		$title = strip_tags($instance['title']);
		$category = strip_tags($instance['category']);
		$post_number = strip_tags($instance['post_number']);
		$post_link = strip_tags($instance['post_link']);

?>
<p>
  <label for="<?php echo $this->get_field_id('title'); ?>">Title:
    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" />
  </label>
</p>
<p>
  <label for="<?php echo $this->get_field_id('category'); ?>">Categories (<code>IDs</code> separated by commas):
  <input class="widefat" id="<?php echo $this->get_field_id('category'); ?>" name="<?php echo $this->get_field_name('category'); ?>" type="text" value="<?php echo attribute_escape($category); ?>" />
  </label>
</p>
<p>
  <label for="<?php echo $this->get_field_id('post_number'); ?>">Number of posts:
  <input class="widefat" id="<?php echo $this->get_field_id('post_number'); ?>" name="<?php echo $this->get_field_name('post_number'); ?>" type="text" value="<?php echo attribute_escape($post_number); ?>" />
  </label>
</p>
<?php
	}

}

register_widget('news1columns');





 // =============================== In The Spotlight Widget (particular category) ======================================

class spotlightpost extends WP_Widget {
	function spotlightpost() {
	//Constructor
		$widget_ops = array('classname' => 'widget Featured Video', 'description' => 'List of In Featured Video in particular category' );
		$this->WP_Widget('spotlight_post', 'Uniq &rarr; Featured Video', $widget_ops);
	}

	function widget($args, $instance) {
	// prints the widget

		extract($args, EXTR_SKIP);
		echo $before_widget;
		$title = empty($instance['title']) ? '&nbsp;' : apply_filters('widget_title', $instance['title']);
		$category = empty($instance['category']) ? '&nbsp;' : apply_filters('widget_category', $instance['category']);
		$post_number = empty($instance['post_number']) ? '&nbsp;' : apply_filters('widget_post_number', $instance['post_number']);
		$post_link = empty($instance['post_link']) ? '&nbsp;' : apply_filters('widget_post_link', $instance['post_link']);

		// if ( !empty( $title ) ) { echo $before_title . $title . $after_title; };
		echo ' <div class="spotlight">';?>
        		
                <h4> <span><?php echo $title; ?></span> </h4>
        		
				<?php 
			        global $post;
			        $latest_menus = get_posts('numberposts='.$post_number.'postlink='.$post_link.'&category='.$category.'');
                    foreach($latest_menus as $post) :
                    setup_postdata($post);
 			    ?>
              		 
                <?php if(get_post_meta($post->ID,'video',true)){?>
                     <div class="video">
                    <?php echo get_post_meta($post->ID,'video',true);?>
                    </div>
                    <?php }?>
                    <p><a class="widget-title" href="<?php the_permalink(); ?>"><?php the_title(); ?> </a></p>
			       
                     
                 <?php endforeach; ?>
                <?php

	    echo '</div>';
		

		echo $after_widget;
	}

	function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['category'] = strip_tags($new_instance['category']);
		$instance['post_number'] = strip_tags($new_instance['post_number']);
		$instance['post_link'] = strip_tags($new_instance['post_link']);
		return $instance;

	}

	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'category' => '', 'post_number' => '' ) );
		$title = strip_tags($instance['title']);
		$category = strip_tags($instance['category']);
		$post_number = strip_tags($instance['post_number']);
		$post_link = strip_tags($instance['post_link']);

?>
<p>
  <label for="<?php echo $this->get_field_id('title'); ?>">Title:
    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" />
  </label>
</p>
<p>
  <label for="<?php echo $this->get_field_id('category'); ?>">Categories (<code>IDs</code> separated by commas):
    <input class="widefat" id="<?php echo $this->get_field_id('category'); ?>" name="<?php echo $this->get_field_name('category'); ?>" type="text" value="<?php echo attribute_escape($category); ?>" />
  </label>
</p>
<p>
  <label for="<?php echo $this->get_field_id('post_number'); ?>">Number of posts:
    <input class="widefat" id="<?php echo $this->get_field_id('post_number'); ?>" name="<?php echo $this->get_field_name('post_number'); ?>" type="text" value="<?php echo attribute_escape($post_number); ?>" />
  </label>
</p>
<?php
	}

}

register_widget('spotlightpost');








// =============================== Flickr widget ======================================

class flickrWidget extends WP_Widget {
	function flickrWidget() {
	//Constructor
		$widget_ops = array('classname' => 'widget Flickr Photos ', 'description' => 'Flickr Photos' );
		$this->WP_Widget('widget_flickrwidget', 'Uniq &rarr; Flickr Photos', $widget_ops);
	}

	function widget($args, $instance) {
	// prints the widget

		extract($args, EXTR_SKIP);
		echo $before_widget;
		$id = empty($instance['id']) ? '&nbsp;' : apply_filters('widget_id', $instance['id']);
		$number = empty($instance['number']) ? '&nbsp;' : apply_filters('widget_number', $instance['number']);

?>

<div class="widget flickr">
			
        <h4><span>Photo Gallery</span></h4>
		  <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=<?php echo $number; ?>&amp;display=latest&amp;size=s&amp;layout=x&amp;source=user&amp;user=<?php echo $id; ?>"></script>  
		 
</div>	</div>

<?php
	}

	function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;
		$instance['id'] = strip_tags($new_instance['id']);
		$instance['number'] = strip_tags($new_instance['number']);
		return $instance;

	}

	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array('title' => '',  'id' => '', 'number' => '') );
		$id = strip_tags($instance['id']);
		$number = strip_tags($instance['number']);
?>

<p>
  <label for="<?php echo $this->get_field_id('id'); ?>">Flickr ID (<a href="http://www.idgettr.com">idGettr</a>):
    <input class="widefat" id="<?php echo $this->get_field_id('id'); ?>" name="<?php echo $this->get_field_name('id'); ?>" type="text" value="<?php echo attribute_escape($id); ?>" />
  </label>
</p>
<p>
  <label for="<?php echo $this->get_field_id('number'); ?>">Number of photos:
    <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo attribute_escape($number); ?>" />
  </label>
</p>
<?php
	}

}

register_widget('flickrWidget');

 
// =============================== Popular Posts Widget ======================================

function PopularPostsSidebar()
{

    $settings_pop = get_option("widget_popularposts");

	$name = $settings_pop['name'];
	$number = $settings_pop['number'];
	if ($name <> "") { $popname = $name; } else { $popname = 'Popular Posts'; }
	if ($number <> "") { $popnumber = $number; } else { $popnumber = '10'; }

?>
 
	<div class="popular_post">
	<h4><span><?php echo $popname; ?></span></h4>
	
		<ul class="news_list">
 			<?php
			global $wpdb;
            $now = gmdate("Y-m-d H:i:s",time());
            $lastmonth = gmdate("Y-m-d H:i:s",gmmktime(date("H"), date("i"), date("s"), date("m")-12,date("d"),date("Y")));
            $popularposts = "SELECT ID, post_title, COUNT($wpdb->comments.comment_post_ID) AS 'stammy' FROM $wpdb->posts, $wpdb->comments WHERE comment_approved = '1' AND $wpdb->posts.ID=$wpdb->comments.comment_post_ID AND post_status = 'publish' AND post_date < '$now' AND post_date > '$lastmonth' AND comment_status = 'open' GROUP BY $wpdb->comments.comment_post_ID ORDER BY stammy DESC LIMIT $popnumber";
            $posts = $wpdb->get_results($popularposts);
            $popular = '';
            if($posts){
                foreach($posts as $post){
	                $post_title = stripslashes($post->post_title);
		               $guid = get_permalink($post->ID);
					   
					      $first_post_title=substr($post_title,0,26);
            ?>
             <?php $post_images = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large'); ?>
            
		        <li>
                      

                       <?php if($post_images[0]){ global $thumb_url; ?>
					  <div class="small_thumb preloader">
                    <a class="widget-title fade_hover" href="<?php echo $guid; ?>">   <img src="<?php echo bloginfo('template_url'); ?>/thumb.php?src=<?php echo $post_images[0];?>&amp;w=60&amp;h=60&amp;zc=1&amp;q=80<?php echo $thumb_url;?>" alt="" title="" />

                     </a>  
					 </div>
            <?php }?>   
                    <div class="rights">
						<h3><a href="<?php echo $guid; ?>" title="<?php echo $post_title; ?>"><?php echo $post_title; ?></a></h3>
						<p class="meta"><?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?></p>
					</div>
                      
                 </li>
            <?php } } ?>

		</ul>
 
 </div>

<?php
}
function PopularPostsAdmin() {

	$settings_pop = get_option("widget_popularposts");

	// check if anything's been sent
	if (isset($_POST['update_popular'])) {
		$settings_pop['name'] = strip_tags(stripslashes($_POST['popular_name']));
		$settings_pop['number'] = strip_tags(stripslashes($_POST['popular_number']));

		update_option("widget_popularposts",$settings_pop);
	}

	echo '<p>
			<label for="popular_name">Title:
			<input id="popular_name" name="popular_name" type="text" class="widefat" value="'.$settings_pop['name'].'" /></label></p>';
	echo '<p>
			<label for="popular_number">Number of popular posts:
			<input id="popular_number" name="popular_number" type="text" class="widefat" value="'.$settings_pop['number'].'" /></label></p>';
	echo '<input type="hidden" id="update_popular" name="update_popular" value="1" />';

}

register_sidebar_widget('Uniq &rarr; Popular Posts', 'PopularPostsSidebar');
register_widget_control('Uniq &rarr; Popular Posts', 'PopularPostsAdmin', 250, 200);


// =============================== Twitter widget ======================================
// Plugin Name: Twitter Widget
// Plugin URI: http://seanys.com/2007/10/12/twitter-wordpress-widget/
// Description: Adds a sidebar widget to display Twitter updates (uses the Javascript <a href="http://twitter.com/badges/which_badge">Twitter 'badge'</a>)
// Version: 1.0.3
// Author: Sean Spalding
// Author URI: http://seanys.com/
// License: GPL
class twitter extends WP_Widget {
	function twitter() {
	//Constructor
		$widget_ops = array('classname' => 'Twitter', 'description' => 'Twitter' );
		$this->WP_Widget('widget_Twidget', 'Uniq &rarr; Twitter', $widget_ops);
	}

	function widget($args, $instance) {
	// prints the widget

		extract($args, EXTR_SKIP);
		echo $before_widget;
		$title = empty($instance['title']) ? '&nbsp;' : apply_filters('widget_title', $instance['title']);
		$account = empty($instance['account']) ? '&nbsp;' : apply_filters('widget_account', $instance['account']);
		$show = empty($instance['show']) ? '&nbsp;' : apply_filters('widget_show', $instance['show']);
		$follow = empty($instance['follow']) ? '&nbsp;' : apply_filters('widget_follow', $instance['follow']);

		 // Output
		echo $before_widget ;

		// start
		echo '<div id="twitter"> <h4><span>'.$title.'</span></h4>';              
		echo '<ul id="twitter_update_list"><li></li></ul>
		      <script type="text/javascript" src="http://twitter.com/javascripts/blogger.js"></script>';
		echo '<script type="text/javascript" src="http://twitter.com/statuses/user_timeline/'.$account.'.json?callback=twitterCallback2&amp;count='.$show.'"></script>';
		echo '</div> </div>';
			
				
		// echo widget closing tag
		echo $after_widget;
	}

	function update($new_instance, $old_instance) {
	//save the widget
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['account'] = strip_tags($new_instance['account']);
		$instance['follow'] = strip_tags($new_instance['follow']);
		$instance['show'] = strip_tags($new_instance['show']);
		return $instance;

	}

	function form($instance) {
	//widgetform in backend
		$instance = wp_parse_args( (array) $instance, array('account'=>'uniqcg', 'title'=>'Twitter Updates', 'show'=>'3' ) );
		$title = strip_tags($instance['title']);
		$show = strip_tags($instance['show']);
		$follow = strip_tags($instance['follow']);
		$account = strip_tags($instance['account']);
?>
<p>
  <label for="<?php echo $this->get_field_id('account'); ?>"><?php  _e('Twitter Account ID')?>:
    <input class="widefat" id="<?php echo $this->get_field_id('account'); ?>" name="<?php echo $this->get_field_name('account'); ?>" type="text" value="<?php echo attribute_escape($account); ?>" />
  </label>
</p>
<p>
  <label for="<?php echo $this->get_field_id('title'); ?>"><?php  _e('Title')?>:
    <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" />
  </label>
</p>

<p>
  <label for="<?php echo $this->get_field_id('show'); ?>"><?php  _e('Show Twitter Posts')?>:
    <input class="widefat" id="<?php echo $this->get_field_id('show'); ?>" name="<?php echo $this->get_field_name('show'); ?>" type="text" value="<?php echo attribute_escape($show); ?>" />
  </label>
</p>

<?php
	}

}

register_widget('twitter');
/*function widget_Twidget_init() {

	if ( !function_exists('register_sidebar_widget') )
		return;

	function widget_Twidget($args) {

		// "$args is an array of strings that help widgets to conform to
		// the active theme: before_widget, before_title, after_widget,
		// and after_title are the array keys." - These are set up by the theme
		extract($args);

		// These are our own options
		$options = get_option('widget_Twidget');
		$account = $options['account'];  // Your Twitter account name
		$title = $options['title'];  // Title in sidebar for widget
		$show = $options['show'];  // # of Updates to show
		$follow = $options['follow'];  // # of Updates to show

        // Output
		echo $before_widget ;

		// start
		echo '<div id="twitter"> <h4><a href="http://www.twitter.com/'.$account.'/" title="'.$follow.'">'.$title.' </a></h4>';              
		echo '<ul id="twitter_update_list"><li></li></ul>
		      <script type="text/javascript" src="http://twitter.com/javascripts/blogger.js"></script>';
		echo '<script type="text/javascript" src="http://twitter.com/statuses/user_timeline/'.$account.'.json?callback=twitterCallback2&amp;count='.$show.'"></script>';
		echo '</div>';
			
				
		// echo widget closing tag
		echo $after_widget;
	}

	// Settings form
	function widget_Twidget_control() {

		// Get options
		$options = get_option('widget_Twidget');
		// options exist? if not set defaults
		if ( !is_array($options) )
			$options = array('account'=>'uniqcg', 'title'=>'Twitter Updates', 'show'=>'3');

        // form posted?
		if ( $_POST['Twitter-submit'] ) {

			// Remember to sanitize and format use input appropriately.
			$options['account'] = strip_tags(stripslashes($_POST['Twitter-account']));
			$options['title'] = strip_tags(stripslashes($_POST['Twitter-title']));
			$options['show'] = strip_tags(stripslashes($_POST['Twitter-show']));
			$options['follow'] = strip_tags(stripslashes($_POST['Twitter-follow']));
			$options['linkedin'] = strip_tags(stripslashes($_POST['Twitter-linkedin']));
			$options['facebook'] = strip_tags(stripslashes($_POST['Twitter-facebook']));
			update_option('widget_Twidget', $options);
		}

		// Get options for form fields to show
		$account = htmlspecialchars($options['account'], ENT_QUOTES);
		$title = htmlspecialchars($options['title'], ENT_QUOTES);
		$show = htmlspecialchars($options['show'], ENT_QUOTES);
		$follow = htmlspecialchars($options['follow'], ENT_QUOTES);

		// The form fields
		echo '<p style="text-align:left;">
				<label for="Twitter-account">' . __('Twitter Account ID:') . '
				<input style="width: 280px;" id="Twitter-account" name="Twitter-account" type="text" value="'.$account.'" />
				</label></p>';
		echo '<p style="text-align:left;">
				<label for="Twitter-title">' . __('Title:') . '
				<input style="width: 280px;" id="Twitter-title" name="Twitter-title" type="text" value="'.$title.'" />
				</label></p>';
		echo '<p style="text-align:left;">
				<label for="Twitter-show">' . __('Show Twitter Posts:') . '
				<input style="width: 280px;" id="Twitter-show" name="Twitter-show" type="text" value="'.$show.'" />
				</label></p>';
		echo '<input type="hidden" id="Twitter-submit" name="Twitter-submit" value="1" />';
	}


	// Register widget for use
	register_sidebar_widget(array('Uniq &rarr; Twitter', 'widgets'), 'Widget_Twidget');

	// Register settings for use, 300x200 pixel form
	register_widget_control(array('Uniq &rarr; Twitter', 'widgets'), 'Widget_Twidget_control', 300, 200);
	
}

// Run code and init
add_action('widgets_init', 'widget_Twidget_init');*/



class artists_search extends WP_Widget {

	function __construct() {
		$widget_ops = array( 'classname' => 'artists_search', 'description' => __( "A list or dropdown of artists" ) );
		$this->WP_Widget('artists', 'Artists search', $widget_ops);
	}

	function widget( $args, $instance ) {
		extract( $args );

		$title = apply_filters('widget_title', empty( $instance['title'] ) ? __( '' ) : $instance['title'], $instance, $this->id_base);
		$c = ! empty( $instance['count'] ) ? '1' : '0';
		$h = ! empty( $instance['hierarchical'] ) ? '1' : '0';
		$d = ! empty( $instance['dropdown'] ) ? '1' : '0';

		echo $before_widget;
		if ( $title )
			echo  '<p>'.$title;

		if ( $d) {
			$cat_args['show_option_none'] = __('Select an Artist');
			$cat_args['exclude'] = "8";
			$cat_args['orderby'] = 'name';
			$cat_args['order'] = 'ASC';

			wp_dropdown_categories(apply_filters('widget_categories_dropdown_args', $cat_args));
			echo ' </p>';
?>

<script type='text/javascript'>
/* <![CDATA[ */
	var dropdown = document.getElementById("cat");
	function onCatChange() {
		if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
			location.href = "<?php echo home_url(); ?>/?cat="+dropdown.options[dropdown.selectedIndex].value;
		}
	}
	dropdown.onchange = onCatChange;
/* ]]> */
</script>

<div class="clear"></div>
<?php
		}


		$cat_args = array('orderby' => 'name', 'show_count' => $c, 'hierarchical' => $h);


		$cat_args['title_li'] = '';
		$cat_arg['echo'] = 0;
		$cat_args['exclude'] = "8";
	$categories =	get_categories(apply_filters('widget_categories_args', $cat_args));
	$categories = $h ? array() : $categories;




  foreach ($categories as $cat) : ?>
	<div class="artists_small">
		<div class="mag preloader">

			<?php 

				$image_url = "/thumb.php?src=".z_taxonomy_image_url($cat->term_id)."&amp;w=200&amp;h=200&amp;zc=1&amp;q=80";
				//echo $image_url;die;
			?>

			<a title="Click to view all artist paintings" rel="bookmark" href="<?php echo get_category_link($cat->term_id);?>" class="preloader" style="background: none repeat scroll 0% 0% transparent;">
				<img class="mag fade_hover" alt="<?php echo $cat->term_id?>" src="<?php bloginfo('template_url'); echo $image_url ?>" style="display: block; visibility: visible; opacity: 1;">
				<div class="artist-name"><?php echo $cat->cat_name.' <span>('.$cat->count.')</span>'; ?></div>
			</a>
		</div>
	</div>
 <?php endforeach; 		

		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['count'] = !empty($new_instance['count']) ? 1 : 0;
		$instance['hierarchical'] = !empty($new_instance['hierarchical']) ? 1 : 0;
		$instance['dropdown'] = !empty($new_instance['dropdown']) ? 1 : 0;

		return $instance;
	}

	function form( $instance ) {
		//Defaults
		$instance = wp_parse_args( (array) $instance, array( 'title' => '') );
		$title = esc_attr( $instance['title'] );
		$count = isset($instance['count']) ? (bool) $instance['count'] :false;
		$hierarchical = isset( $instance['hierarchical'] ) ? (bool) $instance['hierarchical'] : false;
		$dropdown = isset( $instance['dropdown'] ) ? (bool) $instance['dropdown'] : false;
?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('dropdown'); ?>" name="<?php echo $this->get_field_name('dropdown'); ?>"<?php checked( $dropdown ); ?> />
		<label for="<?php echo $this->get_field_id('dropdown'); ?>"><?php _e( 'Display as dropdown' ); ?></label><br />

		<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>"<?php checked( $count ); ?> />
		<label for="<?php echo $this->get_field_id('count'); ?>"><?php _e( 'Show post counts' ); ?></label><br />

		<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('hierarchical'); ?>" name="<?php echo $this->get_field_name('hierarchical'); ?>"<?php checked( $hierarchical ); ?> />
		<label for="<?php echo $this->get_field_id('hierarchical'); ?>"><?php _e( 'Show hierarchy' ); ?></label></p>




<?php
	}

}

add_action( 'widgets_init', create_function('', 'return register_widget("artists_search");') );



?>
