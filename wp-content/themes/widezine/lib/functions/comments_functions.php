<?php /*
URI: http://wphacks.com/log/how-to-add-spam-and-delete-buttons-to-your-blog/
*/ 
function delete_comment_link($id) {
    if (current_user_can('edit_post')) {
        echo '&nbsp;-&nbsp; <a href="'.admin_url("comment.php?action=cdc&c=$id").'">'.get_option('uniq_comment_delete_name').'</a> ';
        echo '&nbsp;-&nbsp; <a href="'.admin_url("comment.php?action=cdc&dt=spam&c=$id").'">'.get_option('uniq_comment_spam_name').'</a>';
    }
}

// Use legacy comments on versions before WP 2.7
add_filter('comments_template', 'old_comments');

function old_comments($file) {

	if(!function_exists('wp_list_comments')) : // WP 2.7-only check
		$file = TEMPLATEPATH . '/comments-old.php';
	endif;

	return $file;
}

// Custom comment loop
function custom_comment($comment, $args, $depth) {	
       $GLOBALS['comment'] = $comment; ?>
	
<li class="comment <?php if (get_comment_ID() % 2==0) echo 'even'; ?>" id="comment-<?php comment_ID() ?>">
		
		    <div class="commentContainer">
				<div class="comimg">
					<?php echo get_avatar( $comment, 60, $template_path . ''.get_bloginfo('template_directory').'/assets/gravatar.png' ); ?>
					<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
				</div>
				<div class="comtext">
					<div class="stylings">
						<p class="desc"><strong><?php comment_author_link() ?></strong>
							<span class="name_f" > <small>(<?php if(!function_exists('how_long_ago')){comment_date('M d, Y'); } else { echo how_long_ago(get_comment_time('U')); } ?>)</small></span>
						 </p>
						
						
							 <?php comment_text() ?>  
							 
						<?php if ($comment->comment_approved == '0') : ?>	
							<p><em><?php echo get_option('uniq_comment_moderation_name'); ?></em></p>	
						<?php endif; ?>
						
						<span class="comm-reply">
							<?php edit_comment_link(''.get_option('uniq_comment_edit_name').'', '&nbsp;&nbsp;&nbsp;', ''); ?>
							<?php delete_comment_link(get_comment_ID()); ?>
						</span>
						
					</div>
				
				</div>

			</div>	
	 
<?php } ?>
