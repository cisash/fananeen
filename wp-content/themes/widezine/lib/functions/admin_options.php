<?php

/* GENERAL SETTINGS */

	$options[] = array(	"type" => "maintabletop");

	    $options[] = array(		"name" => "General Settings",
								"type" => "heading");
			
			// Theme Skin
			
			$options[] = array(	"name" => "Theme skin",
								"type" => "subheadingtop");	

			$options[] = array(	"name" => __("Theme skin"),
								"desc" => __("Select the skin you'd like to use for your magazine."),
								"id" => $shortname."_skin",
								"std" => "",
								"options" => array('Light','Dark'),
								"type" => "select");
								
			$options[] = array(	"type" => "subheadingbottom");
						
		    $options[] = array(	"name" => "Links &amp; Headings Color",
								"type" => "subheadingtop");
								
			
			// Headings color
						
			$options[] = array(	"desc" => "Select a color to be used for links and headings.",
								"id" => $shortname."_alt_stylesheet",
								"std" => "d26630",
								"type" => "text",
								"options" => $alt_stylesheets);
						
			$options[] = array(	"type" => "subheadingbottom");
			
			
			// Additional stylesheet?
			
			$options[] = array(	"name" => "Use additional stylesheet",
								"type" => "subheadingtop");
						
			$options[] = array(	"label" => "Use Custom Stylesheet",
								"desc" => "If you want to make custom design changes using CSS enable and <a href='". $customcssurl . "'>edit custom.css file here</a>.",
								"id" => $shortname."_customcss",
								"std" => "false",
								"type" => "checkbox");	
						
			$options[] = array(	"type" => "subheadingbottom");
			
			
			// Rounded corners?
			
			$options[] = array(	"name" => "Rounded corners",
								"type" => "subheadingtop");
						
			$options[] = array(	"label" => "Enable rounded corners",
								"desc" => "Tick this if you want to have rounded corners on.",
								"id" => $shortname."_corners",
								"std" => "true",
								"type" => "checkbox");	
						
			$options[] = array(	"type" => "subheadingbottom");
			
			
			// About the author box
			
			$options[] = array(	"name" => "About the author box",
								"type" => "subheadingtop");
						
			$options[] = array(	"label" => "Enable about the author box",
								"desc" => "Tick this if you want to display the author box on every post.",
								"id" => $shortname."_about_the_author",
								"std" => "true",
								"type" => "checkbox");	
						
			$options[] = array(	"type" => "subheadingbottom");
			
			
			// Related posts?
			
			$options[] = array(	"name" => "Related posts box",
								"type" => "subheadingtop");
						
			$options[] = array(	"label" => "Enable the related posts box",
								"desc" => "Tick this if you want to display related posts on every post.",
								"id" => $shortname."_related",
								"std" => "true",
								"type" => "checkbox");	
						
			$options[] = array(	"type" => "subheadingbottom");
			
			
			// Cufon?
			
			$options[] = array(	"name" => "Cufon",
								"type" => "subheadingtop");
						
			$options[] = array(	"label" => "Disable Cufon font replacing",
								"desc" => "Tick this if you want to disable cufon for headings.",
								"id" => $shortname."_cufon",
								"std" => "true",
								"type" => "checkbox");	
						
			$options[] = array(	"type" => "subheadingbottom");
			
			
			// Homepage content?
			
			$options[] = array(	"name" => "Homepage content",
								"type" => "subheadingtop");
						
			$options[] = array(	"label" => "Homepage Content",
								"desc" => "Tick this to enable the auto-content on the homepage. Otherwise, it will only be displayed as a widgetized area where you can drag the widgets. The widgets still remain above the content if you are planning to use the content!",
								"id" => $shortname."_homepage_content",
								"std" => "true",
								"type" => "checkbox");	
						
			$options[] = array(	"type" => "subheadingbottom");
			
			
			// Favicon
			
			$options[] = array(	"name" => "Favicon",
								"type" => "subheadingtop");

			$options[] = array(	"desc" => "Paste the full URL for your favicon image here if you wish to show it in browsers. <a href='http://www.favicon.cc/'>Create one here</a>",
								"id" => $shortname."_favicon",
								"std" => "",
								"type" => "text");	
			
			$options[] = array(	"type" => "subheadingbottom");
			
			
			// Logo
			
			$options[] = array(	"name" => "Logo",
								"type" => "subheadingtop");

			$options[] = array(	"name" => "",
								"desc" => "Paste the full URL to your logo image here. <br/ >(eg. http://unithemes.net/theme/logo.png)",
								"id" => $shortname."_logo_url",
								"std" => "",
								"type" => "text");

			$options[] = array(	"name" => "Choose Blog Title over Logo",
				                "desc" => "This option will overwrite your logo selection above - You can <a href='". $generaloptionsurl . "'>change your settings here</a>",
						        "label" => "Show Blog Title + Tagline.",
						        "id" => $shortname."_show_blog_title",
						        "std" => "true",
						        "type" => "checkbox");	

			$options[] = array(	"type" => "subheadingbottom");
			
			
			// Syndication / Feed URL
 	
			$options[] = array(	"name" => "Syndication / Feed URL",
								"type" => "subheadingtop");			
						
			$options[] = array( "desc" => "If you are using a service like Feedburner to manage your RSS feed, enter full URL to your feed into box above. If you'd prefer to use the default WordPress feed, simply leave this box blank. <br/> (eg. http://feeds2.feedburner.com/uniqcg)",
								"id" => $shortname."_feedburner_url",
								"std" => "",
								"type" => "text");	
			
			$options[] = array(	"type" => "subheadingbottom");
			

			// TimThumb
			
			$options[] = array(	"name" => "Timthumb",
								"type" => "subheadingtop");	

			$options[] = array(	"name" => __("Crop position"),
								"desc" => __("Set the default image cropping position."),
								"id" => $shortname."_image_x_cut",
								"std" => "",
								"options" => array('center','top','bottom','left','right','top right','top left','bottom right','bottom left'),
								"type" => "select");
								
			$options[] = array(	"type" => "subheadingbottom");
			

			// Copyright text
			
			$options[] = array(	"name" => "Copyright text",
								"type" => "subheadingtop");

			$options[] = array(	"desc" => "Enter your copyright text to be displayed on the subfooter",
								"id" => $shortname."_copyright_text",
								"std" => "&copy; 2011 WideZine. All rights reserved.",
								"type" => "text");	
			
			$options[] = array(	"type" => "subheadingbottom");
			
			
			// Google Analytics
			
			$options[] = array(	"name" => "Google Analytics",
								"type" => "subheadingtop");

			$options[] = array(	"desc" => "Enter your google analytics code in the textarea below.",
								"id" => $shortname."_analytics",
								"std" => "",
								"type" => "textarea");	
			
			$options[] = array(	"type" => "subheadingbottom");
						
	$options[] = array(	"type" => "maintablebottom");

/* END OF GENERAL SETTINGS */

/* NAVIGATION SETTINGS */
	
	$options[] = array(	"type" => "maintabletop");
											
				
		$options[] = array(	"name" => "Navigation Settings",
						    "type" => "heading");
				
			// Top Strip Navigation
			
			$options[] = array(	"name" => "Top strip header Navigation exclude pages",
								"type" => "subheadingtop");
						
			$options[] = array(	"type" => "multihead");
						
			$options = pages_exclude($options);
									
			$options[] = array(	"type" => "subheadingbottom");
			

 			// Breadcrumbs Navigation
			
			$options[] = array(	"name" => "Breadcrumbs Navigation",
								"type" => "subheadingtop");
						
			$options[] = array(	"label" => "Show breadcrumbs navigation bar",
								"desc" => "i.e. Home > Blog > Title - <a href='". $breadcrumbsurl . "'>Change options here</a>",
								"id" => $shortname."_breadcrumbs",
								"std" => "true",
								"type" => "checkbox");	
						
			$options[] = array(	"type" => "subheadingbottom");

		
	$options[] = array(	"type" => "maintablebottom");
	
/* END OF NAVIGATION SETTINGS */

/* SEO OPTIONS */	
		
	$options[] = array(	"type" => "maintabletop");


				
		$options[] = array(	"name" => "SEO Options",
						    "type" => "heading");
			
			
			// Homepage Tags
			
			$options[] = array(	"name" => "Homepage <code>&lt;meta&gt;</code> tags",
								"type" => "subheadingtop");

			$options[] = array(	"name" => "Meta Description",
								"desc" => "You should use meta descriptions to provide search engines with additional information about topics that appear on your site. This only applies to your home page.",
								"id" => $shortname."_meta_description",
								"std" => "",
								"type" => "textarea");

			$options[] = array(	"name" => "Meta Keywords (comma separated)",
								"desc" => "Meta keywords are rarely used nowadays but you can still provide search engines with additional information about topics that appear on your site. This only applies to your home page.",
								"id" => $shortname."_meta_keywords",
								"std" => "",
								"type" => "text");
									
			$options[] = array(	"name" => "Meta Author",
								"desc" => "You should write your <em>full name</em> here but only do so if this blog is writen only by one outhor. This only applies to your home page.",
								"id" => $shortname."_meta_author",
								"std" => "",
								"type" => "text");
						
			$options[] = array(	"type" => "subheadingbottom");
			
			// noindex tags
			
			$options[] = array(	"name" => "Add <code>&lt;noindex&gt;</code> tags",
								"type" => "subheadingtop");

			$options[] = array(	"label" => "Add <code>&lt;noindex&gt;</code> to category archives.",
								"id" => $shortname."_noindex_category",
								"std" => "true",
								"type" => "checkbox");
									
			$options[] = array(	"label" => "Add <code>&lt;noindex&gt;</code> to tag archives.",
								"id" => $shortname."_noindex_tag",
								"std" => "true",
								"type" => "checkbox");
				
			$options[] = array(	"label" => "Add <code>&lt;noindex&gt;</code> to author archives.",
								"id" => $shortname."_noindex_author",
								"std" => "true",
								"type" => "checkbox");
									
			$options[] = array(	"label" => "Add <code>&lt;noindex&gt;</code> to Search Results.",
								"id" => $shortname."_noindex_search",
								"std" => "true",
								"type" => "checkbox");
				
			$options[] = array(	"label" => "Add <code>&lt;noindex&gt;</code> to daily archives.",
								"id" => $shortname."_noindex_daily",
								"std" => "true",
								"type" => "checkbox");
				
			$options[] = array(	"label" => "Add <code>&lt;noindex&gt;</code> to monthly archives.",
								"id" => $shortname."_noindex_monthly",
								"std" => "true",
								"type" => "checkbox");
				
			$options[] = array(	"label" => "Add <code>&lt;noindex&gt;</code> to yearly archives.",
								"id" => $shortname."_noindex_yearly",
								"std" => "true",
								"type" => "checkbox");
			
			$options[] = array(	"type" => "subheadingbottom");
			
	$options[] = array(	"type" => "maintablebottom");
	
/* END OF SEO OPTIONS */

/* TRANSLATIONS */

	$options[] = array(	"type" => "maintabletop");
		
		
	//////Translations		

	    $options[] = array(	"name" => "Translations",
						    "type" => "heading");
			
			// General Text
			
			$options[] = array(	"name" => "General Text",
								"type" => "subheadingtop");
					
			$options[] = array(	"desc" => "Change Home link text - next to category menu in header",
								"id" => $shortname."_home_name",
								"std" => "Home",
								"type" => "text");
							
			$options[] = array(	"desc" => "Change Search text",
								"id" => $shortname."_search_name",
								"std" => "Search Keyword here",
								"type" => "text");
										
			$options[] = array(	"desc" => "Change Nothing Found for Search text",
								"id" => $shortname."_search_nothing_found",
								"std" => "Nothing found, please search again.",
								"type" => "text");
										
			$options[] = array(	"desc" => "Change Tags text",
								"id" => $shortname."_general_tags_name",
								"std" => "Tags",
								"type" => "text");
				
			$options[] = array(	"type" => "subheadingbottom");
			

			// Archives Text
			
			$options[] = array(	"name" => "Archives Text",
						        "type" => "subheadingtop");
				
			$options[] = array(	"desc" => "Change Browsing Category text",
								"id" => $shortname."_browsing_category",
								"std" => "",
								"type" => "text");
				
			$options[] = array(	"desc" => "Change Browsing Tag text",
								"id" => $shortname."_browsing_tag",
								"std" => "Browsing Tag",
								"type" => "text");
									
			$options[] = array(	"desc" => "Change Browsing Author text",
								"id" => $shortname."_browsing_author",
								"std" => "Browsing Posts of Author",
								"type" => "text");
									
			$options[] = array(	"desc" => "Change Browsing Search text",
								"id" => $shortname."_browsing_search",
								"std" => "Browsing Posts filed under Search Term",
								"type" => "text");
									
			$options[] = array(	"desc" => "Change Browsing Day text",
								"id" => $shortname."_browsing_day",
								"std" => "Browsing Day",
								"type" => "text");
									
			$options[] = array(	"desc" => "Change Browsing Month text",
								"id" => $shortname."_browsing_month",
								"std" => "Browsing Month",
								"type" => "text");
									
			$options[] = array(	"desc" => "Change Browsing Year text",
								"id" => $shortname."_browsing_year",
								"std" => "Browsing Year",
								"type" => "text");
				
			$options[] = array(	"type" => "subheadingbottom");
			
			
			// 404 
			
			$options[] = array(	"name" => "404 Error Text",
								"type" => "subheadingtop");
						
			$options[] = array(	"desc" => "Change 404 error text",
								"id" => $shortname."_404error_name",
								"std" => "Error 404 | Nothing found!",
								"type" => "text");
						
			$options[] = array(	"desc" => "Change 404 error solution text",
								"id" => $shortname."_404solution_name",
								"std" => "Sorry, but you are looking for something that is not here.",
								"type" => "text");
						
			$options[] = array(	"type" => "subheadingbottom");
			
			// Comments
			
			$options[] = array(	"name" => "Comments Text",
								"type" => "subheadingtop");
						
			$options[] = array(	"desc" => "Change password protected text",
								"id" => $shortname."_password_protected_name",
								"std" => "This post is password protected. Enter the password to view comments.",
								"type" => "text");
						
			$options[] = array( "desc" => "Change no responses text",
								"id" => $shortname."_comment_responsesa_name",
								"std" => "No Comments",
								"type" => "text");
				
			$options[] = array( "desc" => "Change one response text",
								"id" => $shortname."_comment_responsesb_name",
								"std" => "One Comment",
								"type" => "text");
				
			$options[] = array( "desc" => "Change multiple responses text, leave % intact!",
								"id" => $shortname."_comment_responsesc_name",
								"std" => "% Comments",
								"type" => "text");
						
			$options[] = array( "desc" => "Change trackbacks text",
								"id" => $shortname."_comment_trackbacks_name",
								"std" => "Trackbacks For This Post",
								"type" => "text");
						
			$options[] = array( "desc" => "Change comment moderation text",
								"id" => $shortname."_comment_moderation_name",
								"std" => "Your comment is awaiting moderation.",
								"type" => "text");
						
			$options[] = array( "desc" => "Change start conversation text",
								"id" => $shortname."_comment_conversation_name",
								"std" => "Be the first to start a conversation",
								"type" => "text");
						
			$options[] = array( "desc" => "Change closed comments text",
								"id" => $shortname."_comment_closed_name",
								"std" => "Comments are closed.",
								"type" => "text");
									
			$options[] = array( "desc" => "Change disabled comments text",
								"id" => $shortname."_comment_off_name",
								"std" => "Comments are off for this post",
								"type" => "text");
						
			$options[] = array( "desc" => "Change leave a reply text",
								"id" => $shortname."_comment_reply_name",
								"std" => "Comments",
								"type" => "text");
				
			$options[] = array( "desc" => "Change 'you must be' text",
								"id" => $shortname."_comment_mustbe_name",
								"std" => "You must be",
								"type" => "text");
				
			$options[] = array( "desc" => "Change 'logged in' text",
								"id" => $shortname."_comment_loggedin_name",
								"std" => "logged in",
								"type" => "text");
						
			$options[] = array( "desc" => "Change 'to post a comment' text",
								"id" => $shortname."_comment_postcomment_name",
								"std" => "to post a comment.",
								"type" => "text");
						
			$options[] = array( "desc" => "Change Logout text",
								"id" => $shortname."_comment_logout_name",
								"std" => "Logout",
								"type" => "text");
						
			$options[] = array( "desc" => "Change name text",
								"id" => $shortname."_comment_name_name",
								"std" => "Name",
								"type" => "text");
						
			$options[] = array( "desc" => "Change mail text",
								"id" => $shortname."_comment_mail_name",
								"std" => "Mail",
								"type" => "text");
						
			$options[] = array( "desc" => "Change website text",
								"id" => $shortname."_comment_website_name",
								"std" => "Website",
								"type" => "text");
						
			$options[] = array( "desc" => "Change add comment text",
								"id" => $shortname."_comment_addcomment_name",
								"std" => "Add Comment",
								"type" => "text");
						
			$options[] = array( "desc" => "Change 'reply' to threaded comment text",
								"id" => $shortname."_comment_justreply_name",
								"std" => "Reply",
								"type" => "text");
						
			$options[] = array( "desc" => "Change 'edit' comment text, only visible to administrators",
								"id" => $shortname."_comment_edit_name",
								"std" => "Edit",
								"type" => "text");
						
			$options[] = array( "desc" => "Change 'delete' comment text, only visible to administrators",
								"id" => $shortname."_comment_delete_name",
								"std" => "Delete",
								"type" => "text");
						
			$options[] = array( "desc" => "Change 'spam' comment text, only visible to administrators",
								"id" => $shortname."_comment_spam_name",
								"std" => "Spam",
								"type" => "text");
						
			$options[] = array(	"type" => "subheadingbottom");
			
			
			// Pagination
			
			$options[] = array(	"name" => "Pagination Text",
								"type" => "subheadingtop");
						
			$options[] = array(	"desc" => "Change first page text",
								"id" => $shortname."_pagination_first_name",
								"std" => "First",
								"type" => "text");
						
			$options[] = array( "desc" => "Change last page text",
								"id" => $shortname."_pagination_last_name",
								"std" => "Last",
								"type" => "text");
						
			$options[] = array(	"type" => "subheadingbottom");
			
			
			// More
			
			$options[] = array(	"name" => "Relative Dates Text",
								"type" => "subheadingtop");
						
			$options[] = array(	"desc" => "Change Posted text",
								"id" => $shortname."_relative_posted",
								"std" => "Posted",
								"type" => "text");
				
			$options[] = array(	"desc" => "Change Ago text",
								"id" => $shortname."_relative_ago",
								"std" => "ago",
								"type" => "text");
				
			$options[] = array(	"desc" => "Change plural text&nbsp;  [ i.e. hour &rarr; hours ]",
								"id" => $shortname."_relative_s",
								"std" => "s",
								"type" => "text");
									
			$options[] = array( "desc" => "Change Year text",
								"id" => $shortname."_relative_year",
								"std" => "year",
								"type" => "text");
									
			$options[] = array( "desc" => "Change Month text",
								"id" => $shortname."_relative_month",
								"std" => "month",
								"type" => "text");
									
			$options[] = array( "desc" => "Change Week text",
								"id" => $shortname."_relative_week",
								"std" => "week",
								"type" => "text");
									
			$options[] = array( "desc" => "Change Day text",
								"id" => $shortname."_relative_day",
								"std" => "day",
								"type" => "text");
									
			$options[] = array( "desc" => "Change Hour text",
								"id" => $shortname."_relative_hour",
								"std" => "hour",
								"type" => "text");
									
			$options[] = array( "desc" => "Change Minute text",
								"id" => $shortname."_relative_minute",
								"std" => "minute",
								"type" => "text");
									
			$options[] = array( "desc" => "Change Second text",
								"id" => $shortname."_relative_second",
								"std" => "second",
								"type" => "text");
									
			$options[] = array( "desc" => "Change Moments text",
								"id" => $shortname."_relative_moments",
								"std" => "moments",
								"type" => "text");
								
			$options[] = array( "desc" => "Change Subscribe to our feed text",
								"id" => $shortname."_subscribe_feed",
								"std" => "Sunbscribe to our feed",
								"type" => "text");
						
			$options[] = array(	"type" => "subheadingbottom");
						
		$options[] = array(	"type" => "maintablebreak");
						
$options[] = array(	"type" => "maintablebottom");

?>
