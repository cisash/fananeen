<?php get_header(); ?>

    <?php if (is_paged()) $is_paged = true; ?>
               
			   <?php if ( get_option( 'uniq_breadcrumbs' )) {  ?>
                    <div class="breadcrumb">
					<?php yoast_breadcrumb('','');  ?>
                	</div>
                <?php } ?>
				
        		<div id="the_body" class="content_index clearfix">
              
    			 <?php if (is_search()) { ?>
                    <h4><span>Search Results for "<?php printf(__('%s'), $s) ?>"</span></h4>
                    <?php } ?>
                      


			<?php if(have_posts()) : ?>
 			<?php while(have_posts()) : the_post() ?>
              <?php $post_images = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large'); ?>
        
                <div id="post-<?php the_ID(); ?>" >
                
				<?php if($post_images[0]){?>
                                <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>" class="preloader"><img src="<?php echo bloginfo('template_url'); ?>/thumb.php?src=<?php echo $post_images[0];?>&amp;w=200&amp; h=200&amp; zc=1&amp;q=80<?php echo $thumb_url;?>" alt="<?php the_title(); ?>"  class="fade_hover" /></a>
                                <?php }?>
				
					<div class="postcontent">
				
					<h3 class="resize"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
                      <?php the_title(); ?>
                      </a></h3>
                      <p class="meta">By <?php the_author_posts_link(); ?> | <?php the_time('F j, Y'); ?> at <?php the_time('g:i a'); ?> | <a href="<?php the_permalink(); ?>#commentarea"><?php comments_number('No comments', 'One comment', '% comments'); ?></a></p>

       
                            
                            
                            <p> <?php echo bm_better_excerpt(375, ''); ?> <a href="<?php the_permalink(); ?>"> more... </a></p>

                       
					</div>
                </div>
				
				<hr />
      <?php endwhile; ?>
      
      
      
      
      <div class="pagination">
        <?php if (function_exists('wp_pagenavi')) { ?>
        <?php wp_pagenavi(); ?>
        <?php } ?>
      </div>
      <?php endif; ?>
			 
      </div> <!-- content #end -->
        
        
		 <?php get_sidebar(); ?>
		
<?php get_footer(); ?>
