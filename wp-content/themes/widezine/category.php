<?php get_header(); ?>

<?php if ( get_option( 'uniq_breadcrumbs' )) {  ?>
   <div class="breadcrumb"><?php yoast_breadcrumb('','');  ?></div>
<?php } ?>


<div class="">
  <?php dynamic_sidebar('Artist page');?>  
</div>
<br />
<div id="the_body">
<?php $category = get_category($cat); ?>


<?php /*
<div class="main_img">
  <div class="zoom">
    <?php $image = z_taxonomy_image_url($category->term_id)? z_taxonomy_image_url($category->term_id) : "http://fananeen.tk/wp-content/uploads/2012/11/artist_sketch.jpg?w=200&h=200&zc=1&q=80"?>
    <a title="<?php echo single_cat_title() ?>" rel="lightbox" href="<?php echo $image ?>" style="background: none repeat scroll 0% 0% transparent;">
      <img class="fade_hover" alt="last painting" src="<?php echo $image ?>" style="opacity: 1;"> 
    </a>
  </div>
</div>

*/ ?>
<div class="artist-data">
  <h1 class="artist-name-single"><?php echo single_cat_title() ?></h1>
  <p><?php echo $category->description; ?></p>


</div>




      <?php 
      $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

      query_posts('posts_per_page=13&cat='.$cat.'&paged=' . $paged.'&orderby=title&order=asc');

      if(have_posts()) : $scounter=0; ?>
        <?php while(have_posts()) : the_post() ?>
  	  <?php $scounter++; ?>
        <?php $post_images = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large'); ?>
        
  		<div id="post-<?php the_ID(); ?>" class="posts_small <?php if ($scounter==1) echo 'first_small'; ?>">
  			<?php if ($post_images[0]) { ?>
  				<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>" class="preloader">
            <img src="<?php echo bloginfo('template_url'); ?>/thumb.php?src=<?php echo $post_images[0];?>&amp;w=200&amp;h=200&amp;zc=1&amp;q=80<?php echo $thumb_url;?>" alt="<?php the_title(); ?>"  class="mag fade_hover" />
          </a>
  			<?php } ?>
  		</div>
  		
        <?php endwhile; ?>

  		<div class="pagination">
  			<?php if (function_exists('wp_pagenavi')) { ?>
  				<?php wp_pagenavi(); ?>
  			<?php } ?>
  		</div>
  	<?php endif; ?>
        
</div> <!-- the_body end -->


<?php get_footer(); ?>

    
