<?php get_header(); ?>
<?php if ( get_option( 'uniq_breadcrumbs' )) {  ?>
	<div class="breadcrumb">
		<?php yoast_breadcrumb('','');  ?>
	</div>
<?php } ?>

<div id="the_body">
	<h4><span><?php echo get_option('uniq_404error_name'); ?></span></h4>
	<p><?php echo get_option('uniq_404solution_name'); ?></p>
</div>
         
<?php get_sidebar(); ?>
<?php get_footer(); ?>
