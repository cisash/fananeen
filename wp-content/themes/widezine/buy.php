<?php
/*
Template Name: buy an item page
*/
?>
<?php get_header(); ?>
<?php global $is_home; ?>
  
<?php if ( get_option( 'uniq_breadcrumbs' )) {  ?>
  <div class="breadcrumb">
     <?php yoast_breadcrumb('','');  ?>
  </div>
<?php } ?>  

<div id="fullwidth">

<?php 
//dynamic_sidebar('Search Artists');

?>



  <?php if(have_posts()) : ?>
    <?php while(have_posts()) : the_post() ?>
        <?php $pagedesc = get_post_meta($post->ID, 'pagedesc', $single = true); ?>

          <div id="post-<?php the_ID(); ?>" >
            <div class="entry"> 
              <?php the_content(); ?>
            </div>    
          </div><!--/post-->
        
 <script type="text/javascript">

          jQuery(document).ready(function() {
            var prmstr = window.location.search.substr(1);
            var prmarr = prmstr.split ("&");
            var params = {};

            for ( var i = 0; i < prmarr.length; i++) {
                var tmparr = prmarr[i].split("=");
                params[tmparr[0]] = tmparr[1];
            }

            painting_id = jQuery("input[name$='painting_id']").parent();
            painting_link = jQuery("input[name$='painting-link']").parent();

            painting_id.hide();
            painting_link.hide();

            painting_id.children('input').val(params.painting_id);
            painting_link.children('input').val(params.painting_link);
          });
  

          </script>


      <?php endwhile; else : ?>
    
          <div class="posts">
            <div class="entry-head"><h2><?php echo get_option('uniq_404error_name'); ?></h2></div>
            <div class="entry-content"><p><?php echo get_option('uniq_404solution_name'); ?></p></div>
          </div>
    
    <?php endif; ?>
</div>
<?php get_footer(); ?>
