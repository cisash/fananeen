<?php get_header(); ?>
<?php if ( get_option( 'uniq_breadcrumbs' )) {  ?>
	<div class="breadcrumb">
	<?php yoast_breadcrumb('','');  ?>
	</div>
<?php 

} 
$category_url = "";
foreach (get_the_category() as $cat) {
  if ($cat->term_id != 8) {
    $category_url =  get_category_link( $cat->term_id );
  }
}

?>		
		 
 		<div id="the_body" >
			<div class="single_post">
      <?php if(have_posts()) : ?> 
         
		  <?php while(have_posts()) : the_post() ?>
				<?php $post_images = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large'); ?>
              <div id="post-<?php the_ID(); ?>" class="posts post_spacer">
             <h1><a href="<?php echo $category_url ?>"><?php the_title(); ?></a></h1>
              
              
               <?php if($post_images[0]){?>
               <div  class="main_img"><div class="zoom"><a href="<?php echo $post_images[0];?>" rel="lightbox" title="<?php the_title(); ?>">  <img src="<?php echo bloginfo('template_url'); ?>/thumb.php?src=<?php echo $post_images[0];?>&amp;w=850&amp;zc=1&amp;q=80<?php echo $thumb_url;?>" alt="<?php the_title(); ?>" class="fade_hover" /> </a></div></div>
                <?php }?>


                <div class="painting-data">
                  
                  <?php the_content(); ?>

                  <?php

                  $price = types_render_field("price");
                  $proprities = array(
                    'Name' => (types_render_field("name-1")),
                    'Size' => (types_render_field("width")).'cmX'.(types_render_field("height")).'cm', 
                    'Material' => (types_render_field("material")),
                    'Price' => $price ? (types_render_field("price")).'$' : "Price upon request",
                    'Year' => (types_render_field("year-1")),
                    'Painting style' => (types_render_field("painting-style")),
                    'Painting reference ID' => (types_render_field("painting_id")),
                    );
                  ?>


                  <div class="painting-details">
                    <h4><span>Painting details</span></h4>
                    <?php
                      foreach ($proprities as $key => $value) :
                        if ($value && !in_array($value, array('cmXcm', '$'))): ?>
                          <p><?php echo $key ?> : <?php echo $value ?></p>
                      <?php
                        endif;
                      endforeach;

                     ?>
                     <?php //<p>painting ID: <?php echo $post->ID </p> ?>
                     <br />
                     <br />
                     <a id="buy-button" href="http://fananeen.com/inquire-about-an-item/?painting_id=<?php echo types_render_field("painting_id") ?>&painting_link=<?php echo get_permalink();?>" >Click here for purchasing this item</a>
                  </div>
                  <p>
                    Check </br>
                    <span class="nav-previous"><?php previous_post_link( '%link', __( '<span class="meta-nav">&larr;</span> Previous', 'twentyeleven' ) ); ?></span>
                    or 
                    <span class="nav-next"><?php next_post_link( '%link', __( 'Next <span class="meta-nav">&rarr;</span>', 'twentyeleven' ) ); ?></span>
                    painting
                  </p>

                </div>



  <?php //if (get_option(uniq_related)) get_related_posts($post); ?>
                        
                  

              </div> <!-- post #end -->

              </div> <!-- single post content #end -->
              <?php if (get_option(uniq_about_the_author)) { ?>
				<?php uniq_author_info(); ?>
				<?php } ?>
              	
                  
          
    </div> <!-- content #end --> 
	
			
  
			  

        <?php /* <div id="comments"><?php comments_template(); ?></div>*/ ?>
      <?php endwhile; ?>
 <?php endif; ?>

  
              
 <?php get_footer(); ?>
